﻿Public Class Form1

    Sub New()

        ' この呼び出しはデザイナーで必要です。
        InitializeComponent()

        ' InitializeComponent() 呼び出しの後で初期化を追加します。
        c = New Class1
    End Sub

#Region "box"

    Private c As Class1
    Private s As Structure1

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Dim o As Object = c
        Dim o2 As Object = o
        My.Application.Log.WriteEntry("c-o:" + o.ToString())
        My.Application.Log.WriteEntry("c-o:" + o.ToString())
        My.Application.Log.WriteEntry("c-o:" + o.ToString())
        My.Application.Log.WriteEntry("c-o2:" + o2.ToString())
        My.Application.Log.WriteEntry("c-o2:" + o2.ToString())
        My.Application.Log.WriteEntry("c-o2:" + o2.ToString())

        My.Application.Log.WriteEntry("------------------")
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Dim o As Object = s
        Dim o2 As Object = o
        My.Application.Log.WriteEntry("s-o:" + o.ToString())
        My.Application.Log.WriteEntry("s-o:" + o.ToString())
        My.Application.Log.WriteEntry("s-o:" + o.ToString())
        My.Application.Log.WriteEntry("s-o2:" + o2.ToString())
        My.Application.Log.WriteEntry("s-o2:" + o2.ToString())
        My.Application.Log.WriteEntry("s-o2:" + o2.ToString())

        My.Application.Log.WriteEntry("------------------")
    End Sub

#End Region

#Region "variable"

    Private Sub sub_obj(ByVal act As Action(Of Object))
        My.Application.Log.WriteEntry("use Action(Of Object)")

        Try
            My.Application.Log.WriteEntry("sub_obj:c == variable covariant ==")
            act(c)
            My.Application.Log.WriteEntry("sub_obj:s == variable invariant? ==")
            act(s)
        Catch ex As Exception
            My.Application.Log.WriteEntry("!!! sub_obj exception !!!")
        End Try

        My.Application.Log.WriteEntry("------------------")
    End Sub

    ' error 
    Private Sub sub_class1(ByVal act As Action(Of Class1))
        My.Application.Log.WriteEntry("use Action(Of Class1)")

        Try
            My.Application.Log.WriteEntry("sub_class1:c == variable invariant ==")
            act(c)
            My.Application.Log.WriteEntry("sub_class1:o(s) == variable contravariant == **error**")
            Dim o As Object = s
            act(DirectCast(o, Class1)) ' error 

        Catch ex As Exception
            My.Application.Log.WriteEntry("!!! sub_class1 exception !!!")
        End Try

        My.Application.Log.WriteEntry("------------------")
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        Dim act_obj As Action(Of Object) = Sub(obj)
                                               My.Application.Log.WriteEntry("obj:" + obj.ToString())
                                           End Sub

        Dim act_class1 As Action(Of Class1) = Sub(class1)
                                                  My.Application.Log.WriteEntry("class1:" + class1.ToString())
                                              End Sub

        Me.sub_obj(act_obj)
        Me.sub_class1(act_class1)
    End Sub

#End Region

#Region "action"

    Private Sub use_sub_obj(ByVal act As Action(Of Class1))
        My.Application.Log.WriteEntry("use Action(Of Class1)")

        Try
            My.Application.Log.WriteEntry("use_sub_obj:class1 == action covariant ==")
            Me.sub_obj(DirectCast(act, Action(Of Object)))
        Catch ex As Exception
            My.Application.Log.WriteEntry("!!! use_sub_obj exception !!!")
        End Try

        My.Application.Log.WriteEntry("++++")
    End Sub

    Private Sub use_sub_class1(ByVal act As Action(Of Object))
        My.Application.Log.WriteEntry("use Action(Of Object)")

        Try
            My.Application.Log.WriteEntry("use_sub_class1:object == action contravariant ==")
            Me.sub_class1(act)
        Catch ex As Exception
            My.Application.Log.WriteEntry("!!! use_sub_class1 exception !!!")
        End Try

        My.Application.Log.WriteEntry("++++")
    End Sub

    ' error 
    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        Dim act_obj As Action(Of Object) = Sub(obj)
                                               My.Application.Log.WriteEntry("obj:" + obj.ToString())
                                           End Sub

        Dim act_class1 As Action(Of Class1) = Sub(class1)
                                                  My.Application.Log.WriteEntry("class1:" + class1.ToString())
                                              End Sub

        Try
            My.Application.Log.WriteEntry("1-act_obj == action contravariant ==")
            Me.use_sub_obj(act_obj)
            My.Application.Log.WriteEntry("2-act_class1 == action invariant == **error**")
            Me.use_sub_obj(act_class1) ' error 
            My.Application.Log.WriteEntry("3-act_obj == action invariant ==")
            Me.use_sub_class1(act_obj)
            My.Application.Log.WriteEntry("4-act_class1 == action covariant == **error**")
            Me.use_sub_class1(DirectCast(act_class1, Action(Of Object))) ' error 
        Catch ex As Exception

        End Try

    End Sub

#End Region
   
End Class
